# -*- coding: utf8 -*-
'''
The wbTieba module.
Author: MC
Data: 2014-2-12
Version: 0.01.00
'''

import urllib
import urllib2
import cookielib
import socket
import simplejson
import re

class baiduC(object):
    Index     = 'http://www.baidu.com/'
    WebLogin  = 'https://passport.baidu.com/v2/api/?login'
    LoginImg  = 'https://passport.baidu.com/?verifypic'
    Token     = 'https://passport.baidu.com/v2/api/?getapi&tpl=pp&apiver=v3&class=login'
    Post      = 'http://tieba.baidu.com/f/commit/post/add'
    Thread    = 'http://tieba.baidu.com/f/commit/thread/add'
    Tbs       = 'http://tieba.baidu.com/dc/common/tbs'
    Vcode     = 'http://tieba.baidu.com/f/user/json_vcode?lm=%s&rs10=2&rs1=0&t=0.7'
    Img       = 'http://tieba.baidu.com/cgi-bin/genimg?%s'

class webUser(object):
    username = ''
    password = ''
    def __init__(self, username, password):
        self.username = username
        self.password = password

class wbTieba(webUser):
    def __init__(self, username, password):
        '''
        生成一个 wbTieba 类的实例
        包括 username 和 password
        通过 cookielib 库操作 cookies
        '''
        # 记录 username 和 password
        webUser.__init__(self, username, password)
        # 自动处理 cookie, 能处理 HTTP response 中的 Set-Cookie
        # 构建一个 CookieJar 类型的实例
        self.cj    = cookielib.CookieJar()
        # build_opener 方法会构造一个 OpenerDirector 类的实例
        # OpenerDirector 类的实例下辖许多 Handle 类
        # 此处的 HTTPCookieProcessor 就是一例
        self.opener= urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        # install_opener 方法的效果是用我们的 OpenerDirector 实例
        # 替换掉 urllib2 默认的 OpenerDirector
        # urllib2.install_opener(self.opener)

    def urlopen(self, url, data = None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT):
        '''
        A new urlopen method that will not conflict with the original one.
        '''
        return self.opener.open(url, data, timeout)

    def is_login(self):
        '''
        baiduC.Tbs 中有 is_login 字段，可以用来判断登录状态
        '''
        return simplejson.loads(self.urlopen(baiduC.Tbs).read())["is_login"]

    def login(self, vf_code = ''):
        '''
        login
        '''
        self.urlopen(baiduC.Index)
        print "%s is logging in ..." % self.username
        self.token       = self.urlopen(baiduC.Token).read()
        self.matchVal    = re.search(u'"token" : "(?P<tokenVal>.*?)"', self.token)
        self.tokenVal    = self.matchVal.group('tokenVal')
        
        self.postData    = {
            'username'   : self.username,
            'password'   : self.password,
            'u'          : 'https://passport.baidu.com/',
            'tpl'        : 'pp',
            'token'      : self.tokenVal,
            'staticpage' : 'https://passport.baidu.com/static/passpc-account/html/v3Jump.html',
            # 'verifycode' : vf_code,
            # 'isPhone'    : 'false',
            # 'charset'    : 'UTF-8',
            # 'callback'   : 'parent.bd__pcbs__ra48vi'
            }
        self.postData = urllib.urlencode(self.postData)

        self.loginRequest = urllib2.Request(baiduC.WebLogin, self.postData)
        self.loginRequest.add_header('Accept','text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
        self.loginRequest.add_header('Accept-Encoding','gzip,deflate,sdch')
        self.loginRequest.add_header('Accept-Language','zh-CN,zh;q=0.8')
        self.loginRequest.add_header('User-Agent','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36')
        self.loginRequest.add_header('Content-Type','application/x-www-form-urlencoded')
        sendPost = self.urlopen(self.loginRequest)
        return self.is_login()

if __name__ == '__main__':
    username = raw_input('Input Username: ')
    password = raw_input('Input password: ')
    user = wbTieba(username, password)
    if user.login():
        print 'Successed'
    else:
        print 'Failed'